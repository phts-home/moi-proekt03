unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Menus, Buttons, Mask, ExtCtrls, toolwin, Clipbrd, math,
  ActnList, ImgList;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    GroupBox1: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    MainMenu1: TMainMenu;
    G1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    Bevel1: TBevel;
    N14: TMenuItem;
    RadioButton3: TRadioButton;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    procedure Edit1Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure N14Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  a, a1, s, c, t, k, a2 : real;

implementation

uses Unit2, Unit3, Unit4;

{$R *.dfm}

procedure TForm1.Edit1Click(Sender: TObject);
begin
edit1.text:='';
end;

procedure TForm1.N1Click(Sender: TObject);
begin
if edit1.text=''
  then form3.showmodal
  else
  begin
  if radiobutton1.checked=true
    then
      begin
        a:=strtofloat(edit1.Text);
        a1:=degtorad(a);
        if abs(sin(a1))=1
          then
            begin
              edit4.text:='�� ��������';
              s:=sin(a1);
              edit2.text:=floattostr(s);
              edit3.text:='0';
              edit5.text:='0'
            end
          else
            if abs(cos(a1))=1
              then
                begin
                  edit5.text:='�� ��������';
                  edit2.text:='0';
                  c:=cos(a1);
                  edit3.text:=floattostr(c);
                  edit4.text:='0'
                end
              else
                begin
                  k:=cos(a1)/sin(a1);
                  c:=cos(a1);
                  s:=sin(a1);
                  t:=sin(a1)/cos(a1);
                  edit2.text:=floattostr(s);
                  edit4.text:=floattostr(t);
                  edit3.text:=floattostr(c);
                  edit5.text:=floattostr(k);
                end;
        edit6.text:=floattostr(a);
        edit7.text:=floattostr(a1);
        edit8.text:=floattostr(degtograd(a));
      end;
  if radiobutton1.checked=true
    then
      begin
        a:=strtofloat(edit1.Text);
        s:=sin(a);
        c:=cos(a);
        t:=sin(a)/cos(a);
        k:=1/t;
        edit2.text:=floattostr(s);
        edit4.text:=floattostr(t);
        edit3.text:=floattostr(c);
        edit5.text:=floattostr(k);
        edit6.text:=floattostr(radtodeg(a));
        edit7.text:=floattostr(a);
        edit8.text:=floattostr(radtograd(a));
      end;
  if radiobutton3.checked=true
    then
    begin
      a:=strtofloat(edit1.Text);
      a1:=gradtorad(a);
      if abs(sin(a1))=1
          then
            begin
              edit4.text:='�� ��������';
              s:=sin(a1);
              edit2.text:=floattostr(s);
              edit3.text:='0';
              edit5.text:='0'
            end
          else
            if abs(cos(a1))=1
              then
                begin
                  edit5.text:='�� ��������';
                  edit2.text:='0';
                  c:=cos(a1);
                  edit3.text:=floattostr(c);
                  edit4.text:='0'
                end
              else
                begin
                  k:=cos(a1)/sin(a1);
                  c:=cos(a1);
                  s:=sin(a1);
                  t:=sin(a1)/cos(a1);
                  edit2.text:=floattostr(s);
                  edit4.text:=floattostr(t);
                  edit3.text:=floattostr(c);
                  edit5.text:=floattostr(k);
                end;
      edit6.text:=floattostr(gradtodeg(a));
      edit7.text:=floattostr(a1);
      edit8.text:=floattostr(a);
    end;
end;
end;

procedure TForm1.N2Click(Sender: TObject);
begin
form4.showmodal;
end;

procedure TForm1.N4Click(Sender: TObject);
begin
form2.showmodal;
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
if edit1.text=''
  then  form3.showmodal
  else
  begin
  if radiobutton1.checked=true
    then
      begin
        a:=strtofloat(edit1.Text);
        a1:=degtorad(a);
        if abs(sin(a1))=1
          then
            begin
              edit4.text:='�� ��������';
              s:=sin(a1);
              edit2.text:=floattostr(s);
              edit3.text:='0';
              edit5.text:='0'
            end
          else
            if abs(cos(a1))=1
              then
                begin
                  edit5.text:='�� ��������';
                  edit2.text:='0';
                  c:=cos(a1);
                  edit3.text:=floattostr(c);
                  edit4.text:='0'
                end
              else
                begin
                  k:=cos(a1)/sin(a1);
                  c:=cos(a1);
                  s:=sin(a1);
                  t:=sin(a1)/cos(a1);
                  edit2.text:=floattostr(s);
                  edit4.text:=floattostr(t);
                  edit3.text:=floattostr(c);
                  edit5.text:=floattostr(k);
                end;
        edit6.text:=floattostr(a);
        edit7.text:=floattostr(a1);
        edit8.text:=floattostr(degtograd(a));
      end;
  if radiobutton2.checked=true
    then
    begin
      a:=strtofloat(edit1.Text);
      s:=sin(a);
      c:=cos(a);
      t:=sin(a)/cos(a);
      k:=1/t;
      edit2.text:=floattostr(s);
      edit4.text:=floattostr(t);
      edit3.text:=floattostr(c);
      edit5.text:=floattostr(k);
      edit6.text:=floattostr(radtodeg(a));
      edit7.text:=floattostr(a);
      edit8.text:=floattostr(radtograd(a));
    end;
  if radiobutton3.checked=true
    then
    begin
      a:=strtofloat(edit1.Text);
      a1:=gradtorad(a);
      if abs(sin(a1))=1
          then
            begin
              edit4.text:='�� ��������';
              s:=sin(a1);
              edit2.text:=floattostr(s);
              edit3.text:='0';
              edit5.text:='0'
            end
          else
            if abs(cos(a1))=1
              then
                begin
                  edit5.text:='�� ��������';
                  edit2.text:='0';
                  c:=cos(a1);
                  edit3.text:=floattostr(c);
                  edit4.text:='0'
                end
              else
                begin
                  k:=cos(a1)/sin(a1);
                  c:=cos(a1);
                  s:=sin(a1);
                  t:=sin(a1)/cos(a1);
                  edit2.text:=floattostr(s);
                  edit4.text:=floattostr(t);
                  edit3.text:=floattostr(c);
                  edit5.text:=floattostr(k);
                end;
      edit6.text:=floattostr(gradtodeg(a));
      edit7.text:=floattostr(a1);
      edit8.text:=floattostr(a);
    end;
end;
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
begin
form4.showmodal;
end;

procedure TForm1.N6Click(Sender: TObject);
begin
clipboard.SetTextBuf(pchar(edit2.text));
end;

procedure TForm1.N7Click(Sender: TObject);
begin
clipboard.SetTextBuf(pchar(edit3.text));
end;

procedure TForm1.N8Click(Sender: TObject);
begin
clipboard.SetTextBuf(pchar(edit4.text));
end;

procedure TForm1.N9Click(Sender: TObject);
begin
clipboard.SetTextBuf(pchar(edit5.text));
end;

procedure TForm1.N11Click(Sender: TObject);
begin
clipboard.SetTextBuf(pchar(edit6.text));
end;

procedure TForm1.N12Click(Sender: TObject);
begin
clipboard.SetTextBuf(pchar(edit7.text));
end;

procedure TForm1.N13Click(Sender: TObject);
begin
clipboard.SetTextBuf(pchar(edit8.text));
end;

procedure TForm1.N14Click(Sender: TObject);
begin
edit1.Text:='';
edit2.Text:='';
edit3.Text:='';
edit4.Text:='';
edit5.Text:='';
edit6.Text:='';
edit7.Text:='';
edit8.Text:='';
end;

procedure TForm1.SpeedButton1Click(Sender: TObject);
begin
clipboard.SetTextBuf(pchar(edit2.text));
end;

procedure TForm1.SpeedButton2Click(Sender: TObject);
begin
clipboard.SetTextBuf(pchar(edit3.text));
end;

procedure TForm1.SpeedButton3Click(Sender: TObject);
begin
clipboard.SetTextBuf(pchar(edit4.text));
end;

procedure TForm1.SpeedButton4Click(Sender: TObject);
begin
clipboard.SetTextBuf(pchar(edit5.text));
end;

procedure TForm1.SpeedButton5Click(Sender: TObject);
begin
clipboard.SetTextBuf(pchar(edit6.text));
end;

procedure TForm1.SpeedButton6Click(Sender: TObject);
begin
clipboard.SetTextBuf(pchar(edit7.text));
end;

procedure TForm1.SpeedButton7Click(Sender: TObject);
begin
clipboard.SetTextBuf(pchar(edit8.text));
end;

end.
