unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, StdCtrls, ExtCtrls, Buttons;

type
  TForm2 = class(TForm)
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    Bevel1: TBevel;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Panel1: TPanel;
    Image2: TImage;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

uses Unit1, Unit4;

{$R *.dfm}

procedure TForm2.Button2Click(Sender: TObject);
begin
form1.close;
form2.Close
end;

procedure TForm2.Button1Click(Sender: TObject);
begin
form2.Close;
form1.show;
end;

procedure TForm2.N2Click(Sender: TObject);
begin
form2.close;
end;

procedure TForm2.N3Click(Sender: TObject);
begin
form4.showmodal;
end;

procedure TForm2.BitBtn1Click(Sender: TObject);
begin
form2.close;
end;

procedure TForm2.BitBtn2Click(Sender: TObject);
begin
form4.showmodal;
end;

end.
